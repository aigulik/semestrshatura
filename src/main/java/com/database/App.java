package com.database;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by SDLAssistant on 30.03.2016.
 */
public class App {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml");
        PersonDao dao = context.getBean(PersonDao.class);
        CompanyDao dao1 = context.getBean(CompanyDao.class);

        Person peter = new Person("Peter", "Sagan");
        Person nasta = new Person("Nasta", "Kuzminova");
        Company company = new Company("Company1", "Kazan");


        // Add new Person records
        dao.save(peter);
        dao.save(nasta);
        dao1.save(company);


        // Count Person records
        System.out.println("Count Person records: " + dao.count());
        System.out.println("Count Company " + dao1.count());

        // Print all records
        List<Person> persons = (List<Person>) dao.findAll();
        for (Person person : persons) {
            System.out.println(person);
        }

        List<Company> companies = (List<Company>) dao1.findAll();
        for (Company company1 : companies){
            System.out.println(company1);
        }

        // Find Person by surname
        System.out.println("Find by surname 'Sagan': "  + dao.findBySurname("Sagan"));
        System.out.println("Find by address" + dao1.findByAddress("Kazan"));

        // Update Person
        nasta.setName("Barbora");
        nasta.setSurname("Spotakova");
        dao.save(nasta);

        System.out.println("Find by id 2: " + dao.findOne(2L));

        // Remove record from Person
        dao.delete(2L);


        // And finally count records
        System.out.println("Count Person records: " + dao.count());

        context.close();
    }
}
