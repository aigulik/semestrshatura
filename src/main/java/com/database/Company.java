package com.database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by ������ on 30.03.2016.
 */
@Entity
@Table(name = "company")

public class Company {

    @Id
    @GeneratedValue
    private Long id;
    private String namecompany;
    private String address;

    public Company() {
    }

    public Company(String namecompany, String address) {
        this.namecompany = namecompany;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return namecompany;
    }

    public void setName(String name) {
        this.namecompany = name;
    }

    public String getSurname() {
        return address;
    }

    public void setSurname(String surname) {
        this.address = surname;
    }

    @Override
    public String toString() {
        return "Company [id=" + id + ", namecompany=" + namecompany + ", address=" + address
                + "]";
    }
}