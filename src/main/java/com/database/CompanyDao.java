package com.database;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ������ on 30.03.2016.
 */
@Repository
public interface CompanyDao extends CrudRepository<Company, Long> {
    public List<Company> findByAddress(String address);
}
