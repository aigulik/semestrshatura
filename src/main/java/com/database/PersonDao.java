package com.database;

import com.database.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SDLAssistant on 30.03.2016.
 */
@Repository
public interface PersonDao extends CrudRepository<Person, Long> {
    public List<Person> findBySurname(String surname);
}

